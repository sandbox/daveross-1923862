<?php

/**
 * @file
 * The Clef module admin interface
 */

/**
 * Define the Clef admin form.
 */
function clef_admin_form($form, &$form_state) {

  $form['clef_application_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Application ID'),
    '#default_value' => check_plain(variable_get('clef_application_id', '')),
    '#size' => 48,
    '#maxlength' => 32,
    '#description' => t("This site's Clef application ID"),
    '#required' => TRUE,

  );

  $form['clef_application_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Application Secret'),
    '#default_value' => check_plain(variable_get('clef_application_secret', '')),
    '#size' => 48,
    '#maxlength' => 32,
    '#description' => t("This site's Clef application secret"),
    '#required' => TRUE,
  );

  $form['#validate'][] = 'clef_admin_form_validate';

  return system_settings_form($form);

}

/**
 * Admin form validation.
 */
function clef_admin_form_validate(&$form, &$form_state) {

  // Require hexadecimal strings.
  $hex_regex = '/^([0-9a-f]{32})/i';

  if (!preg_match($hex_regex, $form_state['values']['clef_application_id'])) {
    form_set_error('clef_application_id', t('The Application ID must be a 128-bit hexadecimal value. Be sure to copy it exactly from the Clef site.'));
  }

  if (!preg_match($hex_regex, $form_state['values']['clef_application_secret'])) {
    form_set_error('clef_application_secret', t('The Application Secret must be a 128-bit hexadecimal value. Be sure to copy it exactly from the Clef site.'));
  }

}
