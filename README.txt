
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage

INTRODUCTION
------------

Current Maintainer: Dave Ross <dave@davidmichaelross.com>

This module integrates with the Clef service, letting users log in with their
Android or iOS smartphone. Clef is a smartphone app that lets you "forget your
passwords" and log into sites as easily as taking a photo.

The Clef module adds a button to Drupal login forms. If you've already logged
into Clef, pressing that button logs you into Drupal automatically. If not,
you'll be asked to scan a QR code with the Clef app on your phone.

Drupal users are matched to their Clef identities by email address, so existing
users can take advantage of Clef right away. Future versions of the Clef module
will support user registration using the Clef app.


INSTALLATION
------------

1. Copy this module's clef/ directory to your sites/SITENAME/modules directory.

2. Enable the module and configure admin/config/services/clef.

If you don't already have a Clef account, download the Clef app for your iPhone
or Android device and sign up.

You will then need to register as a developer at https://developer.clef.io and
create a new application for your site. For the "Application domain", enter the
URL of your Drupal site (i.e. http://www.example.com/) and tell Clef you need
permission to see users' Email, First Name, and Last Name.

If you have any questions about registering with Clef or signing up as a
developer, contact Clef support at info@clef.io

Once your site is registered, Clef will give you "Application ID" and
"Application Secret" codes. Paste these codes in the Clef configuration
screen on your Drupal site, at admin/config/services/clef.

USAGE
-----

This module adds a Clef login button to the normal Drupal login form at /user
and the stock login block. If a user is already logged in with Clef, clicking
the button makes Clef search for a Drupal user with a matching email address.
If one is found, the user is automatically logged in.

If the user is not already logged into Clef, the Clef service's Javascript
displays a QR code which must be scanned with the Clef app on an Android or
iOS device. Doing so logs the user into Clef and any Clef-enabled sites they
visit.
